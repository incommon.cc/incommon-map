<!--
SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

## Parsing Resources

The script `parser.rb` creates a file named `Locations.geojson`, which contains
all the points from the ConcertES and Dewey Maps databases. It parses the .csv
files from the Concertes folder and the .json files from the Dewey folder. To
run it: `ruby parser.rb`

The main files it uses are:

- `Concertes/20200312_EntreprisesSignaletique.csv` which contains the name of
   the points from the Concertes data
- `Concertes/20200312_EntreprisesCoordonnees.csv` which contains the coordinates
   of the points from the Concertes data and
- `Dewey/dewey-maps-markers.json` which contains the data of the points from the
   Dewey data

## Importing to Database

Once you have `Locations.geojson`, connect to the Rails console:

```ruby
# Load the GeoJSON into an Array
locs = JSON.parse(IO.read('doc/import/Locations.geojson'))
# Get related Agent records. You must have run `rails db:seed` beforehand.
dewey = Agent.find_by(name: 'Dewey')
concertes = Agent.find_by(name: 'ConcertES')
# Create Resource records
locs['features'].each do |f|
  agent = f['properties']['source'] == dewey['name'] ? dewey : concertes
  Resource.create(agent_id: agent.id, feature: f)
end
```
### Importing categories

We have a JSON file in French for categories in `doc/import/categories-fr.json`.

From the console:

```ruby
if Category.count == 0
  dewey_taxo = Taxonomy.first
  cats = JSON.parse(IO.read('doc/import/categories-fr.json'))
  cats.each do |cat|
    c = Category.create(name: cat['name'], dewey_id: cat['id'], taxonomy_id: dewey_taxo.id, color: cat['color'])
    cat['sections'].each do |sec|
      Section.create(name: sec['name'], category_id: c.id, dewey_id: sec['id'], color: sec['color'])
    end
  end
  # Now that we have all we need, update resources_sections...
  Resource.all.each do |res|
    sec_ids = res.feature['properties']['categories']
    next if sec_ids.empty?
    sec_ids.each do |id|
      s = Section.find_by(dewey_id: id)
      res.sections << s if s.present?
      res.save
    end
  end
end
```

It's taken into account in `rails db:seed`


### Cleaning up records

Some records need cleanup...

#### Category names

Remove prepended numbers from names:
`Category.all.each { |c| c.update(name: c.name.sub(/^\d. /,'')) if c.name =~ /^\d/ }`

#### Category colors

Match category rank with style color:
```ruby
cat_colors = ['#BCBCBC', '#95a5a6', '#848482', '#948279', '#59706a', '#16a085', '#27ae60', '#2980b9', '#34495e', '#bd3525', '#d35400', '#eead0e']

Category.all.each { |c| c.update!(color: cat_colors.shift) }
```
