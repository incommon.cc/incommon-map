# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class AgentsController < ApplicationController
  before_action :set_agent, only: [:new, :show, :edit, :update, :delete, :destroy]

  # GET /agents
  def index
    @agents = Agent.order(:name).page params[:page]
  end

  # GET /agents/new
  def new
  end

  # POST /agents
  def create
    flash.now[:alert] = 'Agents are created from actual groups for now...'
    render :new
  end

  # GET /agents/:id
  def show
  end

  # GET /agents/:id/edit
  def edit
    flash.now[:notice] = 'Please ask a maintainer to edit this resource!' unless current_agent == @agent
  end

  # PATCH /agents/:id
  def update
    # Check list:
    # 1. Compare records for changes
    # 2. Validate each change
    # 3. Moderate queue or save

    return 403 unless current_agent == @agent

    respond_to do |format|
      if @agent.update(agent_params)
        format.html { redirect_to @agent, notice: 'Merci de votre contribution !' }
        format.json { render :show, status: :ok, location: @agent }
      else
        format.html { render :edit }
        format.json { render json: @agent.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /agents/:id/delete
  def delete
    flash.now[:notice] = 'Please ask a maintainer to delete your Agent!' unless current_agent == @agent
  end

  # DELETE /agents/:id
  def destroy
    return 403 # Yeah, right?
    # Check list
    # 1. User belongs to Agent and is :maintainer?
    unless current_agent == @agent
      msg = 'You must be a maintainer and a leader to delete your Agent!'
      respond_to do |format|
        format.html { redirect_to :show, notice: msg }
        format.json { render json: '{}', status: :forbidden, message: msg }
      end
    else
      @agent.destroy
      respond_to do |format|
        format.html { redirect_to :index, notice: 'OK, resource Agent successfully removed.' }
        format.json { head :no_content }
      end
    end
  end

  private

  def agent_params
    params
      .require(:agent)
      .permit(:uuid,
              :name,
              :summary,
              :description)
  end

  def set_agent
    @agent = Agent.find_by(uuid: params[:id]) || Agent.new
  end
end
