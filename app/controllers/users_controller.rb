# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class UsersController < ApplicationController
  # GET /my/peers
  def index
    @users = current_agent.members
  end

  # GET /my/account
  def show
    @user = current_user
  end
end
