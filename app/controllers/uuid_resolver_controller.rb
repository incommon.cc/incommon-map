# == UUIDResolverController
#
# This controller enables applications to retrieve information about a given UUID.

# It can be used to verify the availability of this UUID in the database, e.g.,
# when a remote Agent assigns an UUID to a record, or when it looks up the
# record for that UUID.
#
# Usually it would either find no matching record (and can thus safely assign
# the UUID) or a single one (and be redirected to the matching resource.).
#
# It's also possible, but unlikely, that the UUID matches more than one record
# (e.g., a Map and a Resource), given the construction of UUIDs. In that case,
# the controller will return the list of matching records.
#
# === Verifying the availability of an UUID
#
# When the call is made to verify an UUID is not yet assigned, a 404 (Not Found)
# response means the UUID is available.
#
# === Identifying an existing record
#
# When the call is made to verify the existence of a record matching this UUID,
# a 302 (Found) response means a single record was found, and the Location
# header gives its URL. If a GET request was made, then the User-Agent will be
# redirected to that Location.
class UUIDResolverController < ApplicationController
  # GET /by-uuid/:uuid
  def new
    @resolver = UUIDResolver.new(params[:uuid])

    case @resolver.count
    when 0
      render json: {
               status: :not_found,
               message: "UUID %<uuid>s is unknown to the system." % { uuid: @resolver.uuid },
               uuid: @resolver.uuid
             },
             status: :not_found
    when 1
      respond_to do |format|
        format.html { redirect_to @resolver.record }
        format.json {
          render json: {
                   status: :found,
                   message: "UUID %<uuid>s was found at %<url>s." % {
                     uuid: @resolver.uuid,
                     url: url_for(@resolver.record)
                   },
                   uuid: @resolver.uuid,
                   location: url_for(@resolver.record)
                 }
        }
      end
    else
      render json: {
               status: :ok,
               message: @resolver.records.to_json
             },
             status: :ok
    end
  rescue ArgumentError => e
    render plain: e.message, status: :unprocessable_entity
  end
end
