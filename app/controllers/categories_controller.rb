# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class CategoriesController < ApplicationController
  before_action :set_category, only: [:new, :show, :edit, :update, :delete, :destroy]

  def index
  end

  def show
    @category = Category.find(params[:id])

    respond_to do |format|
      format.html
      format.js
    end
  end

  def edit
  end

  private

  def set_category
    @category = Category.find_by(id: params[:id]) || Category.new
  end
end
