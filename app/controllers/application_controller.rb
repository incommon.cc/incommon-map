# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class ApplicationController < ActionController::Base
  require 'sso'
  SSO::FromDiscourse.config = Rails.configuration.sso

  before_action :current_user

  protected

  include ApplicationHelper
  include AgentsHelper

  # Set current agent globally
  def current_agent
    @current_agent = Agent.find_by(name: current_agent_name)
  end
  helper_method :current_agent

  # Set current user globally
  def current_user
    @current_user ||= User.find_by(external_id: session[:current_user]) if session[:current_user].present?
  end
  helper_method :current_user

end
