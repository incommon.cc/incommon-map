# SPDX-FileCopyrightText: 2021 IN COMMON Collective
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class Taxonomies::FilterController < ApplicationController
  # GET /taxonomies/:id/filter
  def show
    @taxonomy = Taxonomy.find_by(uuid: params[:id])
    render partial: "taxonomies/filter/taxonomy"
  end
end
