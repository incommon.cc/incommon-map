# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class ResourcesController < ApplicationController
  before_action :set_resource, only: [:show, :delete, :destroy]
#  before_action :set_resource_form, only: [:new, :create, :edit, :update]

  # GET /resources
  def index
    @resources = Resource.order(:uuid).page params[:page]
  end

  # GET /resources/new
  def new
    @resource = ResourceForm.new(current_agent)
  end

  # POST /resources
  def create
    # TODO Background job to list similar items
    # TODO If there's a match, return to user with new record or list of mergeable ones

    @resource = ResourceForm.new(current_agent, resource_form_params)

    respond_to do |format|
      if @resource.save
        format.html { redirect_to @resource.resource, notice: 'Merci de votre contribution !' }
        format.json { render :show, status: :created, location: @resource.resource }
      else
        format.html { render :new }
        format.json { render json: @resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /resources/:id
  def show
  end

  # GET /resources/:id/edit
  def edit
    # TODO Add a moderation queue for unauthorized but valid changes
    flash.now[:notice] = 'Please ask an editor or a maintainer to edit this resource!' unless @resource.agent == current_agent

    Rails.logger.info "EDIT: #{@resource.uuid} #{@resource.name} // #{current_agent.id}"
  end

  # PATCH /resources/:id
  def update
    # Check list:
    # 1. Compare records for changes
    # 2. Validate each change
    # 3. Moderate queue or save

    # TODO: pass this to current_agent and version resource
    return 403 unless current_agent == @resource.agent

    respond_to do |format|
      if @resource.update(resource_params)
        format.html { redirect_to @resource.resource, notice: 'Merci de votre contribution !' }
        format.json { render :show, status: :ok, location: @resource.resource }
      else
        format.html { render :edit }
        format.json { render json: @resource.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /resources/:id/delete
  def delete
    flash.now[:notice] = 'Please ask a maintainer to delete this resource!' unless current_user_maintainer?
  end

  # DELETE /resources/:id
  def destroy
    # Check list
    # 1. User belongs to Agent and is :maintainer?
    if @resource.agent != current_agent
      msg = 'You must be a maintainer to delete resources!'
      respond_to do |format|
        format.html { redirect_to :show, notice: msg }
        format.js   { render json: '{}', status: :forbidden, message: msg }
      end
    else
      @resource.destroy
      respond_to do |format|
        format.html { redirect_to :index, notice: 'OK, resource record successfully remove.' }
        format.js   { head :no_content }
      end
    end
  end

  private

  def resource_form_params
    params
      .require(:resource_form)
      .permit(:agent_id,
              :uuid,
              :name,
              :summary,
              :description,
              :email,
              :website,
              :phone_number,
              :address,
              :postal_code,
              :city,
              :entry_number, # Unused on new resources
              :categories,   #
              :latitude,
              :longitude,
              :source,
              section_ids: [])
  end

  def set_resource
    @resource = Resource.find_by(uuid: params[:id]) || current_agent.resources.build
  end
end
