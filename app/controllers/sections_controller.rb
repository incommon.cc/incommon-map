# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class SectionsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :show

  def show
    @section = Section.find(params[:id])
    respond_to do |format|
      format.html
      format.js
      format.json
    end
  end
end
