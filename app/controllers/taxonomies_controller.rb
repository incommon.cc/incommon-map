# SPDX-FileCopyrightText: 2021 IN COMMON Collective
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class TaxonomiesController < ApplicationController
  # GET /taxonomies
  def index
    @taxonomies = current_agent.taxonomies.order(:uuid).page params[:page]
  end

  # GET /taxonomies/:id
  def show
    @taxonomy = Taxonomy.find_by(uuid: params[:id])
  end
end
