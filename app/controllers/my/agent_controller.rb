# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class My::AgentController < ApplicationController
  # PATCH /my/current_agent
  def switch
    response_status = :unprocessable_entity
    new_agent = current_user.agents.find(params[:agent][:id])
    Rails.logger.info "CURRENT_AGENT #{session[:current_agent]} -> #{new_agent.name}"
    if new_agent.present?
      session[:current_agent] = new_agent.name
      @current_agent = new_agent
      response_status = :ok
      respond_to do |format|
        format.html { redirect_to my_dashboard_url, notice: "L'Agent est maintenant #{new_agent.name}." }
        format.json { render json: { current_agent: session[:current_agent] }, status: response_status }
      end
    else
      respond_to do |format|
        format.html { redirect_to my_dashboard_url, notice: "L'Agent est toujours le meme..." }
        format.json { render json: { current_agent: session[:current_agent] }, status: response_status }
      end
    end

  end

  private

  def agent_params
    params.require(:agent).permit(:id)
  end
end
