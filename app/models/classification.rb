# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class Classification < ApplicationRecord
  belongs_to :resource
  belongs_to :section

  self.primary_key = :resource_id
end
