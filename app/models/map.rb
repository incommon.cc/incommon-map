# SPDX-FileCopyrightText: 2021 IN COMMON Collective
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class Map < ApplicationRecord
  include UUIDParameter
  belongs_to :taxonomy
end
