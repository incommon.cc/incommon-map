# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class Category < ApplicationRecord
  belongs_to :taxonomy
  has_many :sections,
           -> { order(rank: :asc) },
           dependent: :destroy,
           inverse_of: :category

  acts_as_list column: :rank, scope: :taxonomy

  validates :name,
            presence: true,
            uniqueness: { scope: :taxonomy_id },
            length: 3..64
end
