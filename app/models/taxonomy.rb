# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class Taxonomy < ApplicationRecord
  # Universally Unique Identifier :uuid
  include UUIDParameter

  default_scope { order(created_at: :asc) }

  belongs_to :agent
  has_many :categories, -> { order(rank: :asc) }, dependent: :destroy, inverse_of: :taxonomy
  has_many :sections, -> { order(rank: :asv) }, through: :categories

  validates :name,
            presence: true,
            uniqueness: true,
            length: 3..64
  validates :summary,
            length: 0..136
end
