# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class Agent < ApplicationRecord
  has_many :agencies
  has_many :members, through: :agencies, source: :user
  has_many :resources
  has_many :taxonomies
  has_many :categories, through: :taxonomies
  has_many :sections, through: :categories

  def to_param
    uuid
  end

  def to_s
    name
  end

  # Syntactic sugar -- but they're really members.
  def users
    Rails.logger.warn "Calling `Agent#users`! You should be using `Agent#members` instead."
    members
  end
end
