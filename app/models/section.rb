# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class Section < ApplicationRecord
  belongs_to :category
  has_one :taxonomy, through: :category
  has_many :classifications
  has_many :resources, through: :classifications

  acts_as_list column: :rank, scope: :category

  validates :name,
            uniqueness: { scope: :category_id },
            length: { in: 3..64 }
end
