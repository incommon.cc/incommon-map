# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class User < ApplicationRecord
  has_many :agencies
  has_many :agents, through: :agencies, source: :agent

  include UsersHelper

  def avatar_url
    attributes['avatar_url'].present? ?
      attributes['avatar_url'] : '' # default_user_avatar_url
  end
end
