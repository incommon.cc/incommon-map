// SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

// Visit The Stimulus Handbook for more details
// https://stimulusjs.org/handbook/introduction
//

import { Controller } from "stimulus"

export default class extends Controller {
    static targets = [ "category", "deploy", "filter", "section", "toggle" ]

    initialize() {
        console.log("Taxonomy controller initialized.")

        this.overlays = {}
        this.sectionsOnMap = []
    }

    // Load filtering sidebar
    connect() {
        console.log("Taxonomy controller connected.")
        this.taxonomy_uuid = this.data.get('uuid')
        if (this.hasFilterTarget) {
            let url = `/taxonomies/${this.data.get('uuid')}/filter.js`
            console.log(`loading url = ${url}`)
            fetch(url, {
                headers: { accept: 'application/json'}
            })
                .then(response => response.text())
                .then(html => this.filterTarget.innerHTML = html)
        } else {
            console.log("Taxonomy filter is missing")
        }

        // Access the map
        if (this.map = document.querySelector('#map').parentElement.map.map) {
            console.log("Taxonomy Controller connected to this.map")
        } else {
            console.log("Taxonomy Controller could not load map!")
        }
    }


    deploy() {
        console.log(`deploying taxonomy ${this.data.get('uuid')}`);

        fetch(`/taxonomies/${this.data.get('uuid')}.js`)
            .then(response => response.text())
            .then(html => this.deployTarget.innerHTML = html);
    }

    toggle() {
        var cssClass = 'on';
        this.toggleTarget.classList.toggle(cssClass)
        this.filterTarget.parentNode.classList.toggle(cssClass)
    }

    category(event) {
        let catId = (event.target.dataset.taxonomyCategoryId || event.target.parentNode.parentNode.dataset.CategoryId)
        let secId = event.target.dataset.taxonomySectionId
        console.log(`Category: ${catId}/${secId}`)
        var active = event.target.classList.toggle('active')
        if (active) {
            console.log('activated')
        } else {
            console.log('deactivated')
        }
    }

    loadCategory(catId) {
        document.querySelector(`#category-${catId}`).lastChild.childNodes.forEach(function(li) {
            this._mapLayerToggleSection(li.dataset.taxonomySectionId)
        })
    }

    section(event) {
        let secId = event.target.dataset.taxonomySectionId
        console.log(`Section: ${secId} to be loaded`)
        this._loadMarkers(secId)
        this._mapLayerToggleSection(secId)
    }

    _loadMarkers(secId) {
        if (this.overlays[secId] == undefined) {
            console.log(`loading markers for section ${secId}`)
            let overlay = L.layerGroup();
            let markers = L.markerClusterGroup();

            fetch(`/sections/${secId}.json`, {
                headers: { 'X-CSRF-Token': this._csrfToken() }
            })
                .then(response => response.json())
                .then(data => {
                    L.geoJSON(data, {
                        pointToLayer: function (feature, latlng) {
                            return L.marker(latlng, {
                                attribution: feature.source,
                                icon: L.ExtraMarkers.icon({
                                    icon: feature.icon.name,
                                    extraClasses: feature.style.classes,
                                    markerColor: feature.style.color,
                                    shape: feature.icon.shape,
                                    prefix: 'fa',
                                    svg: true
                                })
                            }).bindPopup(feature.popup);
		                    },
                        onEachFeature: (feature, layer) => {
                            layer.on('click', () => this.onClick(layer))
                            layer.addTo(markers)
                        }
                    })
                })
            console.log(`cluster counts ${markers.length} markers`)
            this.overlays[secId] = markers
        }
    }

    onClick(layer) {
        console.log(layer)
    }

    _mapLayerToggleSection(secId) {
        if (this.sectionsOnMap.includes(secId)) {
            console.log(`removing section ${secId} from map`)
            this.map.removeLayer(this.overlays[secId])
            this.sectionsOnMap.pop(secId)
        } else {
            console.log(`adding section ${secId} to the map`)
            this.sectionsOnMap.push(secId)
            this.overlays[secId].addTo(this.map)
        }
    }

    _csrfToken() {
        return document.querySelector('[name=csrf-token]').content
    }
}
