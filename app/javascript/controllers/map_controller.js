// SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

// Visit The Stimulus Handbook for more details
// https://stimulusjs.org/handbook/introduction
//

import { Controller } from "stimulus"

import 'leaflet/dist/leaflet.css'
// Re-uses images from ~leaflet package
import 'leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.webpack.css'

import * as L from 'leaflet'
import 'leaflet-defaulticon-compatibility'
import "leaflet-providers"
import "leaflet.markercluster/dist/leaflet.markercluster.js"

import 'leaflet-extra-markers'
import 'leaflet-easyprint'
import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch';

export default class extends Controller {
    static targets = [ "container" ]

    initialize() {
        console.log("Map controller initialized.")
        this.mapBoxAPIToken = 'pk.eyJ1IjoibmVtYWVsIiwiYSI6ImNrZzBrYjBudTB3bnMyenFmNWtrN3h3bmMifQ.Rkeyhm-9iIQOV7NAMA5LaA'
    }

    connect() {
        var mapbox = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
		        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery <a href="https://www.mapbox.com/">Mapbox</a>',
		        maxZoom: 18,
		        id: 'mapbox/streets-v11',
		        tileSize: 512,
		        zoomOffset: -1,
		        accessToken: this.mapBoxAPIToken
        })

        var stamen_tr = L.tileLayer.provider('Stamen.Toner')
        var stamen_wc = L.tileLayer.provider('Stamen.Watercolor')
        var tiles_osm = L.tileLayer.provider('OpenStreetMap.Mapnik')
        var tiles_sat = L.tileLayer.provider('Esri.WorldImagery')

        const tilemaps = {
            'osm': tiles_osm,
            'mapbox': mapbox,
            'watercolor': stamen_wc,
            'greyscale': stamen_tr,
            'satellite': tiles_sat
        }

        const overlays = {}

        this.map = L.map(this.containerTarget, {
            zoomDelta: 0.5,
            zoomSnap: 0.5,
            layers: [ tiles_osm ],
        }).setView(this._coordinates(), this._zoom());

        L.control.layers(tilemaps, overlays).addTo(this.map)

        /* Show current scale in meters or kilometers */
        L.control.scale({ imperial: false }).addTo(this.map);

        L.DomEvent.on(window, 'hashchange', this.doSomethingCool);

        /* Show a button to print the map */
        L.easyPrint({
            title: 'Print this map',
            position: 'bottomright',
            filename: `incommon-map-detail`,
            exportOnly: true,
            sizeModes: ['A4Portrait', 'A4Landscape']
        }).addTo(this.map);

        /* Add search */
        const provider = new OpenStreetMapProvider();
        const searchControl = new GeoSearchControl({
            provider: provider,
            style: 'button',
            position: 'bottomright'
        });
        this.map.addControl(searchControl);

        // Allow calling the mapController from elsewhere.
        this.element[this.identifier] = this
    }

    disconnect() {
        this.map.remove();
        console.log('Map controller disconnected.')
    }

    doSomethingCool() {
        console.log(window.location.hash)
    }

    _coordinates() {
        return [this.data.get('latitude'), this.data.get('longitude')];
    }

    _zoom() {
        return this.data.get('zoom');
    }

}
