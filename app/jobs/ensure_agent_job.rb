# SPDX-FileCopyrightText: 2021 IN COMMON Collective
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class EnsureAgentJob < ApplicationJob
  queue_as :default

  include AgentsHelper

  def perform(user, groups)
    # Ensure the logged in user has a current agent
    # In order to do this, we first check the existing agents against the user's
    # groups. If none match, we assign the user to the default Anonymous agent.
    existing_agents = Agent.where(name: groups)
    if existing_agents.nil?
      user.agents << default_agent unless user.agents.include? default_agent
    else
      # Update user agents
      user.agents << existing_agents - user.agents
    end
  end
end
