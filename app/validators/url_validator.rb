# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# frozen_string_literal: true

# = URL Validator =
#
# In order to be considered valid, value must be:
#
# - parsable as an URI
# - use the http or https scheme
# - have a hostname
# - have a valid public TLD
#
class UrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless valid_web_address?(value)
      record.errors[attribute] << (options[:message] || 'is an invalid Web URL')
    end
  end

  private

  def valid_web_address?(value)
    uri = URI.parse(value)
    uri.is_a?(URI::HTTP) &&
      uri.host.present? &&
      IANA::TLD.valid?(uri.host.split('.').compact.last)
  rescue URI::InvalidURIError
    false
  end
end
