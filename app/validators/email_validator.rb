# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# frozen_string_literal: true

# = Email Validator =
#
# In order to be considered valid, value must be:
#
# - a valid Email URI
# - parsable as an URI
# - have a hostname
# - have a valid public TLD
#
class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless valid_email_address?(value)
      record.errors[attribute] << (options[:message] || 'is an invalid email address')
    end
  end

  private

  def valid_email_address?(value)
    uri = URI.parse("mailto:#{value}")
    uri.is_a?(URI::MailTo) &&
      uri.to == value &&
      IANA::TLD.valid?(value.split('@', 2).last.split('.').compact.last)
  rescue URI::InvalidURIError, URI::InvalidComponentError
    false
  end
end
