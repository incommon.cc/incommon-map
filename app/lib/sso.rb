# SPDX-FileCopyrightText: 2018-2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# frozen_string_literal: true

# Perform Single Sign-On using Discourse
module SSO
  require 'securerandom'
  require_relative '../../config/initializers/sso_config'
end


