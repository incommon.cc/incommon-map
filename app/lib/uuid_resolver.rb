class UUIDResolver
  # Note the static '4' in the third group: that's the UUID version.
  UUID_V4_REGEX = %r[\A[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[0-9a-f]{4}-[0-9a-f]{12}\z]

  attr_reader :records, :count, :record, :uuid

  def initialize(uuid)
    @uuid   = validate!(uuid)
    @records, @count = resolve!

  end

  def record
    case @count
    when 0
      nil
    else
      records.first
    end
  end

  private

  # List models that have UUIDs
  def public_record_types
    [
      ::Agent,
      ::Map,
      ::Resource,
      ::Taxonomy
    ].freeze
  end

  # Find records with this UUID
  def resolve!
    records = []

    public_record_types.each do |model|
      records << model.find_by(uuid: @uuid)
    end

    [records.compact, records.compact.size]
  end

  # Ensure the passed UUID is correct
  def validate!(uuid)
    validate_uuid_v4(uuid) || raise(ArgumentError.new("You must pass a valid random UUID (https://tools.ietf.org/html/rfc4122)"))
  end

  # Validate a UUID version 4 (random)
  def validate_uuid_v4(uuid)
    uuid = uuid.to_s.downcase
    uuid.match?(UUID_V4_REGEX) ? uuid : false
  end
end
