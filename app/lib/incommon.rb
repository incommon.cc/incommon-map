# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

module INCOMMON
  module_function

  # Return a String representation of the current source code URL
  def repo_url(version)
    case version
    when /^incommon-map (v.*)/
      query="h=#{$1}"
    when /^incommon-map @(.*)/
      query="h=main&id=#{$1}"
    else
      query="h=HEAD"
    end
    URI.parse("https://code.cepheide.org/incommon-map.git/tree/?#{query}").to_s
  end
end
