# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

module MapHelper
  def map_container(map = Map.first)
    return unless map # Fixes a case where there are no maps
    raw tag.div(
          tag.div(id: 'map',
                  data: {
                    target: 'map.container'
                  }),
          data: {
            controller: 'map',
            'map-latitude': map.latitude,
                 'map-longitude': map.longitude,
                 'map-zoom': map.zoom
          })
  end
end
