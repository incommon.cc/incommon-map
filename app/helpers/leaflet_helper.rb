# SPDX-FileCopyrightText: 2021 IN COMMON Collective
#
# SPDX-License-Identifier: AGPL-3.0-or-later

module LeafletHelper
  def json_marker_for(resource, section)
    # Style according to requested section
    marker          = resource.to_geojson
    marker['style'] = {
      classes: "cat#{section.category_id} sec#{section.id}",
      color: section.category.color
    }
    marker['icon']  = {
      name: 'fa-neuter' || section.icon_name || 'fa-circle',
      shape: 'penta' # TODO: change shape according to marker state
    }
    # Render HTML popup
    marker['popup'] = popup_for(resource)

    marker.to_json
  end

  def popup_for(resource)
    render partial: 'resources/popup', locals: { resource: resource }, formats: [:html]
  end
end
