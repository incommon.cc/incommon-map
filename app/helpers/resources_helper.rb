# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

module ResourcesHelper
  # Return a SELECT tag to choose a section in a given taxonomy
  def section_select(taxonomy, selected = nil, options = {})
    grouped_options = []
    tag_name = options[:name] || 'classification[section_ids]'

    taxonomy.categories.each do |c|
      grouped_options << [c.name, c.sections.map { |s| [s.name, s.id] }]
    end

    select_tag(tag_name, grouped_options_for_select(grouped_options, selected), multiple: true)
  end
end
