# SPDX-FileCopyrightText: 2021 IN COMMON Collective
#
# SPDX-License-Identifier: AGPL-3.0-or-later

module SectionsHelper
  # Render a section as a GeoJSON FeatureCollection
  def geojson_feature_collection(section)
    features = section.resources.map { |res| json_marker_for(res, section) }.join(',')

    raw("{ \"type\": \"FeatureCollection\", \"features\": [ #{features} ] }")
  end
end
