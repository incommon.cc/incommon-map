# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

module AgentsHelper
  DEFAULT_AGENT_UUID = "154cd5a4-3341-4c5f-8dd3-805c976c2a26".freeze
  DEFAULT_AGENT_NAME = 'Anonymous'.freeze

  protected

  # Return the name of the current agent from context
  def current_agent_name
    session[:current_agent] || current_user.presence&.agents&.first&.name || default_agent_name
  end

  # Default Agent for users coming from unknown sources
  # This agent uses a different database for writing resources
  def default_agent
    @default_agent ||= Agent.find_by(uuid: DEFAULT_AGENT_UUID) # Anonymous
  end

  # Return the name of the default
  def default_agent_name
    DEFAULT_AGENT_NAME
  end
end
