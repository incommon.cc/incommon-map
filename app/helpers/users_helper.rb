# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

module UsersHelper
  def default_user_avatar_url
    "https://talk.incommon.cc/uploads/default/original/1X/fb0b62dd9ad8917986ea0a901f864f04440de5f5.png"
  end
end
