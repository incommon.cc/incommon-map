# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# coding: utf-8
# frozen_string_literal: true
module Taxonomies::FilterHelper
  def taxonomy_filter
    @taxonomy ||= Taxonomy.first
    html = []
    @taxonomy.categories.each do |cat|
      list = []
      cat.sections.each do |sec|
        list << tag.li(h("#{sec.rank}. #{sec.name}"), id: "section-#{sec.rank}", data: { action: "click->taxonomy#section", target: 'taxonomy.section', 'taxonomy-section-id': sec.id })
      end
      html << tag.li(h("#{cat.rank}. #{cat.name}") << tag.ol(list.join.html_safe),
                     id: "category-#{cat.rank}",
                     data: { action: "click->taxonomy#category", target: 'taxonomy.category', 'taxonomy-category-id': cat.id })
    end
    raw(tag.nav(tag.ol(html.join.html_safe), id: "taxonomy-#{@taxonomy.uuid}"))
  end
end
