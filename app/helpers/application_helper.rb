# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

module ApplicationHelper
  # Set CSS classes on body tag
  def body_classes(*args)
    classes = args || []
    classes << controller_name
    classes << action_name
    ' class="'.html_safe << classes.join(' ') << '"'.html_safe
  end

  # Markdown helper
  # Always use all extensions. Additional parser and render options may be
  # passed as a second argument.
  #
  # @param markdown (String) a string to parse as Markdown source
  # @param options (String or Array) an optional parser/renderer option
  # @return String HTML-formatted from Mardkown source
  def m(markdown, options = nil)
    tag.div(
      CommonMarker.render_doc(
        markdown.to_s,
        options.to_a + default_commonmarker_options,
        default_commonmarker_extensions
      ).to_html.html_safe, class: 'markdown')
  end

  # Link to current application version
  def version_link
    version = begin
                IO.read('.app-version')&.strip
              rescue
                'HEAD'
              end

    link_to(version, INCOMMON.repo_url(version), title: 'Read source code')
  end

  private

  # CommonMarker extensions
  # See https://github.com/gjtorikian/commonmarker#extensions
  def default_commonmarker_extensions
    [
      :table,
      :tasklist,
      :strikethrough,
      :autolink,
      :tagfilter
    ]
  end

  # CommonMarker options
  # See https://github.com/gjtorikian/commonmarker#options
  def default_commonmarker_options
    [
      #:HARDBREAKS, # only seems to work with render_html, but then all others
                    # only work with render_doc
      :FOOTNOTES,
      :SMART,
      :STRIKETHROUGH_DOUBLE_TILDE,
      :VALIDATE_UTF8
    ]
  end
end
