# SPDX-FileCopyrightText: 2004-2020 David Heinemeier Hansson
# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
# SPDX-License-Identifier: MIT

# Puma can serve each request in a thread from an internal thread pool.
# The `threads` method setting takes two numbers: a minimum and maximum.
# Any libraries that use thread pools should be configured to match
# the maximum value specified for Puma. Default is set to 5 threads for minimum
# and maximum; this matches the default thread size of Active Record.
#

require 'rails'

max_threads_count = ENV.fetch("RAILS_MAX_THREADS") { 5 }
min_threads_count = ENV.fetch("RAILS_MIN_THREADS") { max_threads_count }
threads min_threads_count, max_threads_count

app_dir = File.expand_path('../..', __FILE__)
shared_dir = "#{app_dir}/tmp"

# Specifies the `environment` that Puma will run in.
#
environment ENV.fetch("RAILS_ENV") { "development" }

# Specifies the `port` that Puma will listen on to receive requests; default is 3000.
#
if Rails.env.production?
  # Bind to a (faster) socket
  bind "unix://#{shared_dir}/sockets/puma.sock";
  # Log production errors
  stdout_redirect "#{app_dir}/log/puma.stdout.log", "#{app_dir}/log/puma.stderr.log", true
  # Specifies the number of `workers` to boot in clustered mode.
  # Workers are forked web server processes. If using threads and workers together
  # the concurrency of the application would be max `threads` * `workers`.
  # Workers do not work on JRuby or Windows (both of which do not support
  # processes).
  #
  workers ENV.fetch("WEB_CONCURRENCY") { 2 }
  # Use the `preload_app!` method when specifying a `workers` number.
  # This directive tells Puma to first boot the application and load code
  # before forking the application. This takes advantage of Copy On Write
  # process behavior so workers use less memory.
  #
  preload_app!
else
  port ENV.fetch("PORT") { 3000 }
  # Do not redirect STDOUT so we can use byebug!
  stdout_redirect nil, "#{app_dir}/log/puma.stderr.log", true
  workers 1 # Enable web console debug during development
end

# Specifies the `pidfile` that Puma will use.
pidfile ENV.fetch("PIDFILE") { "#{shared_dir}/pids/server.pid" }
# Keep puma state
state_path "#{shared_dir}/pids/puma.state"

# Allow puma to be restarted by `rails restart` command.
plugin :tmp_restart
# Better integration with systemd
plugin :systemd
