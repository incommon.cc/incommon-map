# SPDX-FileCopyrightText: 2020 IN COMMON Collective <collective@incommon.cc>
# SPDX-License-Identifier: AGPL-3.0-or-later

Rails.application.routes.draw do

  get 'sections/show'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root to: 'welcome#index'

  # Agent routes
  resources :agents do
    shallow do
      # Classification routes
      resources :taxonomies do
        member do
          get :filter, to: 'taxonomies/filter#show'
        end
        resources :categories do
          resources :sections
        end
      end
      # Resource routes
      resources :resources
    end
  end

  # User routes
  get '/my/account', to: 'users#show', as: 'account'
  patch '/my/current_agent', to: 'my/agent#switch', as: 'agent_switch'
  get '/my/dashboard', to: 'welcome#dashboard'
  get '/my/peers', to: 'users#index', as: 'users'

  # UUID Resolver
  get '/by-uuid/:uuid', to: 'uuid_resolver#new', as: 'uuid_resolver'

  # Discourse SSO
  get 'authenticate(/:token)', to: 'welcome#authenticate', as: 'authenticate'
  get 'logout', to: 'welcome#logout'
end
