if Rails.env.production?
  # Only serve session cookies over HTTPS
  Rails.application.config.session_store :cookie_store, key: 'map_session', expire_after: 7.days, httponly: true, secure: true
else
  Rails.application.config.session_store :cookie_store, key: 'map_session', expire_after: 7.days
end
