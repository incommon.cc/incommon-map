# SPDX-FileCopyrightText: 2018-2020 IN COMMON Collective <collective@incommon.cc>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# frozen_string_literal: true

# See lib/sso/from_discourse.rb
# module SSO
#   class FromDiscourse
#     class << self
#       attr_accessor :config
#     end
#   end
# end

require 'sso/from_discourse'



