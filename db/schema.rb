# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_22_050635) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "agencies", force: :cascade do |t|
    t.bigint "agent_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["agent_id", "user_id"], name: "index_agencies_on_agent_id_and_user_id", unique: true
    t.index ["agent_id"], name: "index_agencies_on_agent_id"
    t.index ["user_id"], name: "index_agencies_on_user_id"
  end

  create_table "agents", force: :cascade do |t|
    t.string "name"
    t.uuid "uuid"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_agents_on_name", unique: true
    t.index ["uuid"], name: "index_agents_on_uuid", unique: true
  end

  create_table "categories", force: :cascade do |t|
    t.string "name", limit: 64
    t.string "summary", limit: 136
    t.text "description"
    t.bigint "taxonomy_id", null: false
    t.string "color", limit: 25
    t.integer "rank"
    t.integer "sections_count", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "dewey_id"
    t.index ["dewey_id"], name: "index_categories_on_dewey_id", unique: true
    t.index ["taxonomy_id"], name: "index_categories_on_taxonomy_id"
  end

  create_table "classifications", id: false, force: :cascade do |t|
    t.bigint "resource_id", null: false
    t.bigint "section_id", null: false
  end

  create_table "map_taxonomies", force: :cascade do |t|
    t.bigint "map_id", null: false
    t.bigint "taxonomy_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["map_id"], name: "index_map_taxonomies_on_map_id"
    t.index ["taxonomy_id"], name: "index_map_taxonomies_on_taxonomy_id"
  end

  create_table "maps", force: :cascade do |t|
    t.uuid "uuid", null: false
    t.decimal "latitude", precision: 9, scale: 7
    t.decimal "longitude", precision: 10, scale: 7
    t.integer "zoom", default: 13
    t.bigint "taxonomy_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["taxonomy_id"], name: "index_maps_on_taxonomy_id"
    t.index ["uuid"], name: "index_maps_on_uuid", unique: true
  end

  create_table "resources", force: :cascade do |t|
    t.uuid "uuid"
    t.jsonb "feature", default: {"geometry"=>{"type"=>"Point", "coordinates"=>[0, 0]}, "properties"=>{"city"=>"", "name"=>"", "srid"=>4326, "email"=>"", "source"=>"incommon", "address"=>"", "website"=>"", "categories"=>[], "description"=>"", "postal_code"=>"", "entry_number"=>"", "phone_number"=>""}}
    t.bigint "agent_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["agent_id"], name: "index_resources_on_agent_id"
    t.index ["feature"], name: "index_resources_on_feature", using: :gin
    t.index ["uuid"], name: "index_resources_on_uuid", unique: true
  end

  create_table "sections", force: :cascade do |t|
    t.string "name", limit: 64
    t.string "summary", limit: 136
    t.text "description"
    t.bigint "category_id", null: false
    t.string "color", limit: 25
    t.integer "rank", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "dewey_id"
    t.string "icon_name", limit: 32, default: "circle"
    t.index ["category_id"], name: "index_sections_on_category_id"
    t.index ["dewey_id"], name: "index_sections_on_dewey_id", unique: true
  end

  create_table "taxonomies", force: :cascade do |t|
    t.string "name", limit: 64
    t.string "summary", limit: 64
    t.text "description"
    t.uuid "uuid"
    t.integer "categories_count", default: 0
    t.bigint "agent_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["agent_id"], name: "index_taxonomies_on_agent_id"
    t.index ["name"], name: "index_taxonomies_on_name", unique: true
    t.index ["uuid"], name: "index_taxonomies_on_uuid", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "username"
    t.string "email"
    t.bigint "external_id"
    t.string "avatar_url"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["external_id"], name: "index_users_on_external_id", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "agencies", "agents"
  add_foreign_key "agencies", "users"
  add_foreign_key "categories", "taxonomies"
  add_foreign_key "map_taxonomies", "maps"
  add_foreign_key "map_taxonomies", "taxonomies"
  add_foreign_key "maps", "taxonomies"
  add_foreign_key "resources", "agents"
  add_foreign_key "sections", "categories"
  add_foreign_key "taxonomies", "agents"
end
