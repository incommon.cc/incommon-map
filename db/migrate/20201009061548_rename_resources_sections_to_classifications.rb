class RenameResourcesSectionsToClassifications < ActiveRecord::Migration[6.0]
  def change
    rename_table :resources_sections, :classifications
  end
end
