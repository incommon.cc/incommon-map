class CreateCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :categories do |t|
      t.string :name, limit: 64
      t.string :summary, limit: 136
      t.text :description
      t.references :taxonomy, null: false, foreign_key: true
      t.string :color, limit: 25
      t.integer :rank
      t.integer :sections_count, default: 0

      t.timestamps
    end
  end
end
