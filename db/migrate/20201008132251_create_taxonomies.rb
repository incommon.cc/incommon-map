class CreateTaxonomies < ActiveRecord::Migration[6.0]
  def change
    create_table :taxonomies do |t|
      t.string :name, limit: 64
      t.string :summary, limit: 64
      t.text :description
      t.uuid :uuid
      t.integer :categories_count, default: 0
      t.references :agent, null: false, foreign_key: true

      t.timestamps
    end
    add_index :taxonomies, :name, unique: true
    add_index :taxonomies, :uuid, unique: true
  end
end
