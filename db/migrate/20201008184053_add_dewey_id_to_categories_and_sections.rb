class AddDeweyIdToCategoriesAndSections < ActiveRecord::Migration[6.0]
  def change
    add_column :categories, :dewey_id, :integer
    add_column :sections, :dewey_id, :integer
    add_index :categories, :dewey_id, unique: true
    add_index :sections, :dewey_id, unique: true
  end
end
