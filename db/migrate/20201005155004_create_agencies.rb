class CreateAgencies < ActiveRecord::Migration[6.0]
  def change
    create_table :agencies do |t|
      t.references :agent, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.integer :roles, default: 0, null: false, limit: 2

      t.timestamps
    end
    add_index :agencies, [:agent_id, :user_id], unique: true
  end
end
