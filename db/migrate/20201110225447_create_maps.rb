class CreateMaps < ActiveRecord::Migration[6.0]
  def change
    create_table :maps do |t|
      t.uuid :uuid, null: false, unique: true
      t.decimal :latitude, precision: 9, scale: 7
      t.decimal :longitude, precision: 10, scale: 7
      t.integer :zoom, default: 13
      t.references :taxonomy, null: false, foreign_key: true

      t.timestamps
    end
    add_index :maps, [:uuid], unique: true
  end
end
