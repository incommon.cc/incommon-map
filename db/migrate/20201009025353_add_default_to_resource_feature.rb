class AddDefaultToResourceFeature < ActiveRecord::Migration[6.0]
  def up
    change_column_default :resources, :feature,
                          {
                            "geometry": {
                                          "type": "Point",
                                          "coordinates": [0,0]
                                        },
                            "properties": {
                                            "name":"",
                                            "description":"",
                                            "address":"",
                                            "postal_code":"",
                                            "city":"",
                                            "email":"",
                                            "phone_number":"",
                                            "website":"",
                                            "categories":[],
                                            "source":"incommon",
                                            "entry_number":nil,
                                            "srid":4326
                                         }
                          }
    add_index :resources, :feature, using: :gin
  end
  def down
    change_column_default :resources, :feature, nil
    remove_index :resources, :feature
  end
end
