class AddSectionIcon < ActiveRecord::Migration[6.1]
  def change
    add_column :sections, :icon_name, :string, limit: 32, default: 'fa-neuter'
  end
end
