class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :username
      t.string :email
      t.bigint :external_id
      t.string :avatar_url

      t.timestamps
    end
    add_index :users, [:external_id], unique: true
    add_index :users, [:email], unique: true
  end
end
