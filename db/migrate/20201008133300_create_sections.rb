class CreateSections < ActiveRecord::Migration[6.0]
  def change
    create_table :sections do |t|
      t.string :name, limit: 64
      t.string :summary, limit: 136
      t.text :description
      t.references :category, null: false, foreign_key: true
      t.string :color, limit: 25
      t.integer :rank, default: 0

      t.timestamps
    end
  end
end
