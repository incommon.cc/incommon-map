class CreateJoinTableResourceSections < ActiveRecord::Migration[6.0]
  def change
    create_join_table :resources, :sections do |t|
      # t.index [:resource_id, :section_id]
      # t.index [:section_id, :resource_id]
    end
  end
end
