class CreateAgents < ActiveRecord::Migration[6.0]
  def change
    create_table :agents do |t|
      t.string :name
      t.uuid :uuid

      t.timestamps
    end
    add_index :agents, [:name], unique: true
    add_index :agents, [:uuid], unique: true
  end
end
