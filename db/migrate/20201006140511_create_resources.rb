class CreateResources < ActiveRecord::Migration[6.0]
  def change
    create_table :resources do |t|
      t.uuid :uuid
      t.jsonb :feature
      t.references :agent, null: false, foreign_key: true

      t.timestamps
    end
    add_index :resources, [:uuid], unique: true
  end
end
