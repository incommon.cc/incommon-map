# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Create a bunch of Agents
{
  'Dewey': '6347f151-6782-437c-8bae-55730672a76f',
  'PS': '3211da93-69fb-4d9b-825c-6ab51784fd18',
  'incommon': '262ea4f5-186d-483b-8414-025f23b65eb4',
  'ConcertES': '066e5968-03c3-4c15-bd5b-f73a10b5e9f1',
  'Anonymous': '154cd5a4-3341-4c5f-8dd3-805c976c2a26'
}.each do |agent_name, uuid|
  Agent.find_or_create_by(name: agent_name, uuid: uuid)
end

dewey = Agent.find_by(uuid: '6347f151-6782-437c-8bae-55730672a76f') or die('Dewey Agent should be available by now!')

# Create a default taxonomy
Taxonomy.find_or_create_by(
  name: 'Dewey Maps Taxonomy',
  summary: 'Original taxonomy used in Belgique, Mode d\'Emploi',
  description: '## Dewey Maps Taxonomy',
  uuid: '2519915f-d19c-4281-b758-f5ddb889d7fa',
  agent_id: dewey.id
)

# Create French categories and sections
if Category.count == 0
  dewey_taxo = Taxonomy.first
  cats = JSON.parse(IO.read('doc/import/categories-fr.json'))
  cats.each do |cat|
    c = Category.create(name: cat['name'], dewey_id: cat['id'], taxonomy_id: dewey_taxo.id, color: cat['color'])
    cat['sections'].each do |sec|
      Section.create(name: sec['name'], category_id: c.id, dewey_id: sec['id'], color: sec['color'])
    end
  end
  # Now that we have all we need, update resources_sections...
  Resource.all.each do |res|
    sec_ids = res.feature['properties']['categories']
    next if sec_ids.empty?
    sec_ids.each do |id|
      s = Section.find_by(dewey_id: id)
      res.sections << s if s.present?
      res.save
    end
  end
end

# Create default map
if Map.count == 0
  Map.create(uuid: "1a42651e-3fe8-4e83-bfcd-f14bb8d0c713", latitude: 50.8503396, longitude: 4.3517103, zoom: 13, taxonomy_id: 1)
end

# TODO: Fix section icons
# This thing below was a start with maki icons but we need more...
# And fork-awesome is missing some as well, so we need to create them.
=begin
if Section.first.icon_name == 'circle'
  {
    # cat-30
    148: 'fa-campsite',
    158: 'hospital',
    130: 'landmark-JP',
    198: 'shelter',
    150: 'lodging',
    149: 'playground',
    165: 'residential-community',
    131: 'home',
    168: 'residential-community',
    164: 'residential-community',
    114: 'home',
    # cat-31
    170: 'convenience',
    180: 'sushi',
    155: 'garden',
    135: 'shop',
    115: 'drinking-water',
    154: 'restaurant',
    118: 'restaurant',
    28: 'recycling',
    27: 'garden-center',
    185: 'farm',
    182: 'fast-food',
    123: 'beer',
    # cat-32
    210: 'square',
    156: 'clothing-store',
    46: 'gift',
    144: 'waterfall',
    104: 'clothing-store',
    207: 'toilet',
    # cat-33
    18: 'hospital',
    145: 'doctor',
    157: 'doctor',
    192: 'shelter',
    199: 'defibrillator',
    190: 'shelter',
    143: 'heart',
    208: 'hospital',
    191: 'heart',
    # cat-34
    699: 'shop',
    138: 'logging',
    136: 'recycling',
    50: 'library',
    132: 'garden-center',
    42: 'hardware',
    113: 'building',
    206: 'clothing-store',
    47: 'recycling',
    205: 'recycling',
    120: 'shop',
    # cat-35
    128: 'garden-center',
    121: 'waste-basket',
    116: 'garden',
    133: 'park',
    194: 'natural',
    129: 'picnic-site',
    125: 'recycle',
    127: 'natural',
    # cat-36
    102: 'ranger-station',
    19: 'town',
    17: 'skateboard',
    14: ''


  }.each do |id, name|
    Section.find_by(dewey_id: id).update(icon_name: name)
  end
end
=end
